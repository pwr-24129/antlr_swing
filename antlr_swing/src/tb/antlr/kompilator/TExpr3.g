tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}

prog    
  : (e+=expr | d+=decl | e+=block)* -> program(name={$e}, deklaracje={$d})
  ;
  
decl
  : ^(VAR id=ID) { declareVariable($id.text); } -> dek(name={getLocalName($id.text)})
  | ^(PODST id=ID value=expr) { checkVariable($id.text); } -> setVariable(name={getLocalName($id.text)}, value={$value.st})
  ;
  catch [RuntimeException ex] {errorID(ex, $id);}

expr    
  : ^(PLUS  e1=expr e2=expr)        -> calc(operation={"ADD"}, a={$e1.st}, b={$e2.st})
  | ^(MINUS e1=expr e2=expr)        -> calc(operation={"SUB"}, a={$e1.st}, b={$e2.st})
  | ^(MUL   e1=expr e2=expr)        -> calc(operation={"MUL"}, a={$e1.st}, b={$e2.st})
  | ^(DIV   e1=expr e2=expr)        -> calc(operation={"DIV"}, a={$e1.st}, b={$e2.st})
  | ID { checkVariable($ID.text); } -> getVariable(name={getLocalName($ID.text)})
  | INT {numer++;}                  -> int(i={$INT.text},j={numer.toString()})
  ;
  
block
  : LB { enterScope(); }
  | RB { leaveScope(); }
  ;
    