package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.*;

public class MyTreeParser extends TreeParser {
	
	private GlobalSymbols mGlobalSymbols = new GlobalSymbols();
	private LocalSymbols mLocalSymbols = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer a, Integer b) {
		return a + b;
	}
	
	protected Integer sub(Integer a, Integer b) {
		return a - b;
	}
	
	protected Integer mul(Integer a, Integer b) {
		return a * b;
	}
	
	protected Integer div(Integer a, Integer b) {
		return a / b;
	}
	
	protected Integer mod(Integer a, Integer b) {
		return a % b;
	}
	
	protected void declareGlobalVariable(String name) {
		mGlobalSymbols.newSymbol(name);
	}
	
	protected void setGlobalVariable(String name, Integer value) {
		mGlobalSymbols.setSymbol(name, value);
	}
	
	protected Integer getGlobalVariable(String name) {
		return mGlobalSymbols.getSymbol(name);
	}
	
	protected void enterScope() {
		mLocalSymbols.enterScope();
	}
	
	protected void leaveScope() {
		mLocalSymbols.leaveScope();
	}
	
	protected void declareLocalVariable(String name) {
		if (!mLocalSymbols.hasSymbol(name)) {
			mLocalSymbols.newSymbol(name);
		}
	}
	
	protected void setLocalVariable(String name, Integer value) {
		if (!mLocalSymbols.hasSymbol(name)) {
			declareLocalVariable(name);
		}
		mLocalSymbols.setSymbol(name, value);
	}
	
	protected Integer getLocalVariable(String name) {
		if (!mLocalSymbols.hasSymbol(name)) {
			throw new RuntimeException("Symbol \"" + name + "\" not found.");
		}
		return mLocalSymbols.getSymbol(name);
	}
}
