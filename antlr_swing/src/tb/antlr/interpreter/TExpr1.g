tree grammar TExpr1;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;
  superClass=MyTreeParser;
}

@header {
  package tb.antlr.interpreter;
}

prog
  :(expr
  | ^(PRINT e=expr) {drukuj($e.text + " = " + $e.out.toString());}
  | decl
  | block)*
  ;

expr returns [Integer out]
  : ^(PLUS  e1=expr e2=expr) { $out = add($e1.out, $e2.out); }
  | ^(MINUS e1=expr e2=expr) { $out = sub($e1.out, $e2.out); }
  | ^(MUL   e1=expr e2=expr) { $out = mul($e1.out, $e2.out); }
  | ^(DIV   e1=expr e2=expr) { $out = div($e1.out, $e2.out); }
  | ^(MOD   e1=expr e2=expr) { $out = mod($e1.out, $e2.out); }
  | INT                      { $out = getInt($INT.text); }
  | ID                       { $out = getLocalVariable($ID.text); }
  ;

decl
  : ^(VAR name=ID)            { declareLocalVariable($name.text); }
  | ^(PODST name=ID value=expr) { setLocalVariable($name.text, $value.out); }
  ;
  
block
  : LB { enterScope(); }
  | RB { leaveScope(); }
  ;